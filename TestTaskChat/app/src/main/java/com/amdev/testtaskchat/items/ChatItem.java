package com.amdev.testtaskchat.items;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amdev.testtaskchat.R;
import com.amdev.testtaskchat.models.ChatMessage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatItem extends LinearLayout {
    private TextView messIn, timeIn, messOut, timeOut;

    public ChatItem(Context context, ChatMessage message) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.chat_list_item, this, true);
        init();
        updateLayout(message);
    }

    private void init() {
        messIn = (TextView) findViewById(R.id.messIn);
        timeIn = (TextView) findViewById(R.id.timeIn);
        messOut = (TextView) findViewById(R.id.messOut);
        timeOut = (TextView) findViewById(R.id.timeOut);
        setEnabled(false);
        setOnClickListener(null);
    }

    public void updateLayout(ChatMessage message) {
        setVisibilities(message);
        setData(message);
    }

    private void setVisibilities(ChatMessage message) {
        if(message.isMy()) {
            messOut.setVisibility(VISIBLE);
            timeOut.setVisibility(VISIBLE);
            messIn.setVisibility(GONE);
            timeIn.setVisibility(GONE);
        } else {
            messOut.setVisibility(GONE);
            timeOut.setVisibility(GONE);
            messIn.setVisibility(VISIBLE);
            timeIn.setVisibility(VISIBLE);
        }
    }

    private void setData(ChatMessage message) {
        if(message.isMy()) {
            messOut.setText(getTextWithRedTags(message.getText()));
            timeOut.setText(message.getTime());
        } else {
            messIn.setText(getTextWithRedTags(message.getText()));
            timeIn.setText(message.getTime());
        }
    }

    private SpannableStringBuilder getTextWithRedTags(String text) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        Pattern p = Pattern.compile("#[A-Za-z]+", Pattern.CASE_INSENSITIVE);
        Matcher matcher = p.matcher(text);
        while (matcher.find()){
            spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.rgb(255, 0, 0)),
                    matcher.start(),matcher.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }
        return spannableStringBuilder;
    }

}