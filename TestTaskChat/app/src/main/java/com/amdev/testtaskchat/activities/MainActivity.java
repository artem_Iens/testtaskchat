package com.amdev.testtaskchat.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.amdev.testtaskchat.R;
import com.amdev.testtaskchat.adapters.ChatAdapter;
import com.amdev.testtaskchat.helpers.DateHelper;
import com.amdev.testtaskchat.models.ChatMessage;
import com.amdev.testtaskchat.orm.DataSource;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends ActionBarActivity {
    private Button btnSendMessage;
    private EditText etMessage;
    private ListView chatList;
    private ChatAdapter adapter;
    private ArrayList<ChatMessage> messages;
    private Random random;

    private String[] randomMessages = {"Sweet #dreams are made of this, who am I to disagree",
                                        "Travel the world and #seven seas",
                                       "Everybody's looking for #something",
                                       "Some of them #want to use you, some of them want to get #used by you",
                                       "Some of them want to #abuse you, some of them want to be obused"};

    private Handler msgGenerateHandler = new Handler();
    private Handler setListStateToBottomHandler = new Handler();
    private Runnable msgGenerateRunnable = new Runnable() {
        @Override
        public void run() {
            addMsg(new ChatMessage(getRandomMessage(), DateHelper.getCurrentTime(), false));
            msgGenerateHandler.postDelayed(msgGenerateRunnable, getRandomDelay());
        }
    };
    private Runnable setListStateToBottomRunnable = new Runnable() {
        @Override
        public void run() {
            chatList.setSelection(messages.size() -1);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setListeners();
    }

    private void init() {
        btnSendMessage = (Button) findViewById(R.id.btnSendMessage);
        etMessage = (EditText) findViewById(R.id.etMessage);
        chatList = (ListView) findViewById(R.id.chatList);
        messages = DataSource.getAllMessages();
        adapter = new ChatAdapter(this, messages);
        chatList.setAdapter(adapter);
        random = new Random();
        if(messages.size() > 0) chatList.setSelection(messages.size() -1);
    }

    private void setListeners() {
        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMsg(obtainMsg());
            }
        });
        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                btnSendMessage.setEnabled(!TextUtils.isEmpty(s) && s.toString().trim().length() > 0);
            }
        });
        //instead of catching keyboard shown/hidden event, cause I didn't found easy way to do this.
        etMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chatList != null && adapter != null && messages != null) {
                    //if listView scrolled to bottom, up items with keyboard
                    if(chatList.getLastVisiblePosition() == messages.size() - 1) {
                        setListStateToBottomHandler.postDelayed(setListStateToBottomRunnable, 200);
                    }
                }
            }
        });
        //to handle btn done pressed event, when horizontal orientation and keyboard matches screen
        etMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if(!TextUtils.isEmpty(etMessage.getText()) && etMessage.getText().toString().trim().length() > 0) {
                        addMsg(obtainMsg());
                    }
                }
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clean:
                DataSource.clearChatMessagesTable();
                messages.clear();
                if(adapter != null) adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private ChatMessage obtainMsg() {
        ChatMessage message = new ChatMessage(etMessage.getText().toString(),
                DateHelper.getCurrentTime(), true);
        etMessage.setText("");
        return message;
    }

    private void addMsg(ChatMessage message) {
        ChatMessage.createInDB(message);
        messages.add(message);
        adapter.notifyDataSetChanged();
        if(chatList.getLastVisiblePosition() == (messages.size() -2) || message.isMy()) {
            chatList.setSelection(messages.size() - 1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        msgGenerateHandler.postDelayed(msgGenerateRunnable, getRandomDelay());
    }

    private int getRandomDelay() {
        return 1000 * random.nextInt(10) + 1000;
    }

    private String getRandomMessage() {
        return randomMessages[random.nextInt(5)];
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(msgGenerateHandler != null) msgGenerateHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(msgGenerateHandler != null) msgGenerateHandler.removeCallbacksAndMessages(null);
        if(setListStateToBottomHandler != null) setListStateToBottomHandler.removeCallbacksAndMessages(null);
    }
}
