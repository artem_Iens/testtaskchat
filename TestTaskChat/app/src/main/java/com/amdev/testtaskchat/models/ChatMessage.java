package com.amdev.testtaskchat.models;

import com.amdev.testtaskchat.orm.DBManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.SQLException;

@DatabaseTable(tableName="messages")
public class ChatMessage {
    @DatabaseField
    private String text;
    @DatabaseField
    private String time;
    @DatabaseField
    private boolean isMy;

    public ChatMessage() {
        super();
    }

    public ChatMessage(String text, String time, boolean isMy) {
        this.text = text;
        this.time = time;
        this.isMy = isMy;
    }

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }

    public boolean isMy() {
        return isMy;
    }

    public static void createInDB(ChatMessage chatMessage) {
        try {
            DBManager.getInstance().getHelper().getChatMessageDao().create(chatMessage);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
