package com.amdev.testtaskchat;

import android.app.Application;

import com.amdev.testtaskchat.orm.DBManager;

public class ChatApplication extends Application {

    public void onCreate() {
        super.onCreate();
        DBManager.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DBManager.getInstance().release();
    }
}
