package com.amdev.testtaskchat.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.amdev.testtaskchat.items.ChatItem;
import com.amdev.testtaskchat.models.ChatMessage;

import java.util.ArrayList;

public class ChatAdapter  extends BaseAdapter {
    private Context context;
    private ArrayList<ChatMessage> messages;

    public ChatAdapter(Context context, ArrayList<ChatMessage> data) {
        this.context = context;
        messages = data;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ChatItem chatItem = (ChatItem) convertView;
        if(chatItem == null) {
            chatItem = new ChatItem(context, messages.get(position));
        } else {
            chatItem.updateLayout(messages.get(position));
        }
        return chatItem;
    }
}
