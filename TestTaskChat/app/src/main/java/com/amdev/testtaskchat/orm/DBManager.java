package com.amdev.testtaskchat.orm;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

public class DBManager {
    private static volatile DBManager instance;
    private DBHelper helper;

    private DBManager() {

    }

    public static DBManager getInstance() {
        if (instance == null) {
            synchronized (DBManager.class) {
                if (instance == null) {
                    instance = new DBManager();
                }
            }
        }
        return instance;
    }

    public void init(Context context){
        if(helper == null)
            helper = OpenHelperManager.getHelper(context, DBHelper.class);
        helper.getWritableDatabase();
    }

    public void release(){
        if(helper != null)
            OpenHelperManager.releaseHelper();
    }

    public DBHelper getHelper() {
        return helper;
    }
}
