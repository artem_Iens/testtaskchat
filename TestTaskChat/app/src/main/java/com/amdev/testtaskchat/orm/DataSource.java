package com.amdev.testtaskchat.orm;

import com.amdev.testtaskchat.models.ChatMessage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DataSource {

    public static ArrayList<ChatMessage> getAllMessages() {
        ArrayList<ChatMessage> result = new ArrayList<ChatMessage>();
        try {
            List<ChatMessage> messages = DBManager.getInstance().getHelper().getChatMessageDao().queryForAll();
            result.addAll(messages);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void clearChatMessagesTable() {
        try {
            DBManager.getInstance().getHelper().clearChatMessagesTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
