package com.amdev.testtaskchat.helpers;

import android.text.format.DateFormat;

public class DateHelper {

    public static String getCurrentTime() {
        CharSequence time = DateFormat.format("HH:mm:ss", System.currentTimeMillis());
        return time.toString();
    }

}
